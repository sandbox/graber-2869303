<?php

namespace Drupal\live_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\live_test\Test\LiveTestManager;
use \Drupal\Core\State\StateInterface;

/**
 * Test form class.
 */
class LiveTestForm extends FormBase {

  /**
   * Hinderpremie test plugin manager.
   *
   * @var \Drupal\hinderpremie_tests\Test\LiveTestManager
   */
  protected $testManager;

  /**
   * Drupal state object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(LiveTestManager $testManager, StateInterface $state) {
    $this->testManager = $testManager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.ltest'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hinderpremie_tests_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $tests = $this->testManager->getDefinitions();
    if (empty($tests)) {
      drupal_set_message($this->t('There are no tests defined.'), 'error');
    }
    else {
      foreach ($tests as $id => $test) {
        $options[$id] = $test['label'];
      }

      $form['tests'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Tests'),
        '#options' => $options,
      ];

      $form['execute'] = [
        '#type' => 'submit',
        '#value' => $this->t('Execute'),
      ];
    }

    $last_results = $this->state->get('live_test_result');
    if (!empty($last_results)) {
      $form['results'] = [
        '#type' => 'details',
        '#open' => FALSE,
        '#title' => $this->t('Results of the last test'),
      ];
      $form['results']['results'] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Group'),
          $this->t('Method'),
          $this->t('Status'),
        ],
        '#rows' => [],
        '#empty' => $this->t('There are no results.'),
      ];

      foreach ($last_results as $result) {
        $status = ($result['message'] == 'Passed') ? 'pass' : 'fail';
        $form['results']['results']['#rows'][] = [
          'data' => [
            $tests[$result['group']]['label'],
            $result['method'],
            $result['message'],
          ],
          'class' => [$status],
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty(array_filter($form_state->getValue('tests')))) {
      $form_state->setError($form['tests'], $this->t('Select at least one test to perform.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tests = array_filter($form_state->getValue('tests'));
    $operations = [];
    foreach ($tests as $id) {
      $instance = $this->testManager->createInstance($id);
      foreach (get_class_methods($instance) as $method) {
        if (substr($method, 0, 4) === 'test') {
          $operations[] = [
            'plugin_id' => $id,
            'method' => $method,
          ];
        }
      }
    }

    if (!empty($operations)) {
      $batch = [
        'title' => t('Performing live tests.'),
        'operations' => [
          [
            ['Drupal\live_test\Test\LiveTestBatch', 'operation'],
            ['operations' => $operations],
          ],
        ],
        'finished' => ['Drupal\live_test\Test\LiveTestBatch', 'finished'],
        'progress_message' => $this->t('Estimated time left: @estimate, elapsed: @elapsed.'),
        'file' => drupal_get_path('module', 'gipod') . '/includes/gipod.batch.inc',
      ];
      batch_set($batch);
    }
    else {
      drupal_set_message($this->t('No test callbacks found.'));
    }
  }

}
