<?php

namespace Drupal\live_test\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Live Test annotation object.
 *
 * @Annotation
 */
class LiveTest extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the action plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
