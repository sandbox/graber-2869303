<?php

namespace Drupal\live_test\Test;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for a VBO Action plugin.
 */
interface LiveTestInterface extends PluginInspectionInterface {
}
