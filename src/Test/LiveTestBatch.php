<?php

namespace Drupal\live_test\Test;

/**
 * Contains Batch API callbacks.
 */
abstract class LiveTestBatch {

  /**
   * Execute single test operation.
   */
  public static function operation($operations, &$context) {
    if (!isset($context['sandbox']['total'])) {
      $context['sandbox']['total'] = count($operations);
      $context['sandbox']['current'] = 0;
    }

    if (isset($operations[$context['sandbox']['current']])) {
      $operation = $operations[$context['sandbox']['current']];

      $testManager = \Drupal::service('plugin.manager.ltest');
      $instance = $testManager->createInstance($operation['plugin_id']);
      try {
        $results = $instance->{$operation['method']}();
      }
      catch (\Exception $e) {
        $results = [$e->getMessage() . ', file: ' . $e->getFile() . ', line: ' . $e->getLine()];
      }

      if (isset($results)) {
        if (empty($results)) {
          $results = [t('Failed')];
        }
        elseif ($results === TRUE) {
          $results = [t('Passed')];
        }

        if (!is_array($results)) {
          $results = [$results];
        }

        foreach ($results as $result) {
          $context['results'][] = [
            'group' => $operation['plugin_id'],
            'method' => $operation['method'],
            'message' => $result,
          ];
        }
      }

      $context['sandbox']['current']++;

      $context['message'] = t('Testing @operation of plugin @plugin (@current of @total)', [
        '@operation' => $operation['method'],
        '@plugin' => $instance->label(),
        '@current' => $context['sandbox']['current'],
        '@total' => $context['sandbox']['total'],
      ]);
      $context['finished'] = $context['sandbox']['current'] / $context['sandbox']['total'];
    }
  }

  /**
   * Finished callback.
   */
  public static function finished($success, $results, $operations) {
    $state = \Drupal::service('state');
    $state->set('live_test_result', $results);
  }

}
