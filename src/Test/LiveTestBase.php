<?php

namespace Drupal\live_test\Test;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\Schema\SchemaCheckTrait;

/**
 * Base class for Hinderpremie test.
 */
abstract class LiveTestBase extends PluginBase implements LiveTestInterface, ContainerFactoryPluginInterface {

  use SchemaCheckTrait;

  /**
   * Config schemas to check.
   *
   * @var array
   */
  protected $schemas = [];

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, TypedConfigManagerInterface $typedConfigManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $configFactory;
    $this->typedConfigManager = $typedConfigManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('config.typed')
    );
  }

  /**
   * Get plugin label.
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Test module schema.
   */
  public function testSchema() {
    if (!empty($this->schemas)) {
      foreach ($this->schemas as $schema_name) {

        $typedDataManager = $this->typedConfigManager->get($schema_name)->getTypedDataManager();
        $config_data = $this->configFactory->get($schema_name)->get();

        $output = [];
        $result = $this->checkConfigSchema($typedDataManager, $schema_name, $config_data);
        if (is_array($result)) {
          foreach ($result as $variable_name => $error) {
            $output[] = $variable_name . ': ' . $error;
          }
        }
        elseif ($result === FALSE) {
          $output[] = t('No schema for @schema_name found.', ['@schema_name' => $schema_name]);
        }

        $empty = [];
        foreach ($config_data as $name => $value) {
          if ($value === '') {
            $empty[] = $name;
          }
        }
        if (!empty($empty)) {
          $output[] = t('Empty configuration: @missing', ['@missing' => implode(', ', $empty)]);
        }
        if (!empty($output)) {
          return $output;
        }
        else {
          return TRUE;
        }
      }
    }
  }

}
