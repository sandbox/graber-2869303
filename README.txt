INTRODUCTION
------------

This module provides a new plugin type LTest and a base class
LiveTestBase that can be implemented by other modules and
an UI to perform tests.

The tests are preformed on live environment so it's possible
not only to check code in an isolated environment but also check
all configuration and live operation of services envolved.


INSTALLATION
------------

Install as any other module.


USAGE
-----

In a custom module create a class in folder src/Plugin/LTest with annotation:

 * @LiveTest(
 *   id = "[plugin_id]",
 *   label = @Translation("Some label for test group"),
 * )

the plugin can extend the provided base class LiveTestBase that already
implements config schema testing.

To implement config schema testing,
just add a class property called "schemas":

  /**
   * {@inheritdoc}
   */
  protected $schemas = [
    'some_module.config',
  ];

Same as in case of PHPUnit tests, the test method names should
begin with the "test" word in lowercase, an example:

  /**
   * Test a preconfigured external webAPI service.
   */
  public function testService() {
    $service = \Drupal::service('some_service');

    // Try to connect.
    $response = $service->connect();
    if (empty($response)) {
      return [t('Error: returned token string empty')];
    }
    else {
      return TRUE;
    }
  }

Test method returns TRUE if the test is passed and
an array of errors (single string also supported) it the test failed.
If the executed code caused an exception, it'll be caught
by the executing handler and displayed as a result.

Tests can be performed using UI: /admin/config/development/live-test
